<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Add a new product</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
	<h1>Add a new product</h1>
	<form:form action="saveProduct" modelAttribute="products" method="POST">
		<form:hidden path="id"/>
		<div class=form-group>
		<label for="itemName">Item Name</label>
		    <form:input class="form-control" placeholder="Enter the name of the item" path="itemName"/>
		</div>
		<div class=form-group>
		<label for="itemType">Item Type</label>
		    <form:input class="form-control" placeholder="Enter the type of item" path="itemType"/>
		</div>
		<div class=form-group>
		<label for="itemColour">Item Size</label>
		    <form:input class="form-control"  placeholder="Enter the size of the item" path="itemSize"/>
		</div>
		<div class=form-group>
		<label for="itemColour">Item Colour</label>
		    <form:input class="form-control"  placeholder="Enter the colour of the item" path="itemColour"/>
		</div>
		<div class=form-group>
		<label for="itemSpec">Item Spec</label>
		    <form:input class="form-control" placeholder="Enter the spec of the item" path="itemSpec"/>
		</div>
		<div class=form-group>
		<label for="itemLocation">Item Location</label>
		    <form:input class="form-control" placeholder="Enter the location of the item" path="itemLocation"/>
		</div>
		<div class=form-group>
		<label for="beacon">Beacon</label>
		    <form:input class="form-control" placeholder="Enter the id of the beacon" path="beacon"/>
		</div>
		<div class=form-group>
		<label>Customer</label>
		    <form:input class="form-control" placeholder="Enter customer details" path="customer"/>
		</div>
		  <button type="submit" class="btn btn-primary">Add / Update Product</button>
	</form:form>
</body>
</html>