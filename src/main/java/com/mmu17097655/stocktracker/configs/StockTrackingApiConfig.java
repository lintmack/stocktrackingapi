package com.mmu17097655.stocktracker.configs;

import java.beans.PropertyVetoException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

import com.mchange.v2.c3p0.ComboPooledDataSource;

@Configuration
@EnableWebMvc
@EnableTransactionManagement
@ComponentScan(basePackages="com.mmu17097655.stocktracker")


/**
 * 
 * This class contains configuration settings for the web service.
 * 
 * @author linton
 * @version 1.0
 * @since 2018-09-12
 *
 */
public class StockTrackingApiConfig implements WebMvcConfigurer {


	@Bean
	public ViewResolver viewResolver() {
		
		InternalResourceViewResolver vR = new InternalResourceViewResolver();
		
		vR.setPrefix("/WEB-INF/view/");
		vR.setSuffix(".jsp");
		
		return vR;
	}
	
	@Bean
	public DataSource appDataSource() {
		
		// creates a connection pool
		ComboPooledDataSource appDataSource = new ComboPooledDataSource();

		// sets up the jdbc driver
		try {
			appDataSource.setDriverClass("com.mysql.jdbc.Driver");		
		}
		catch (PropertyVetoException e) {
			throw new RuntimeException(e);
		}
		
		
		//AWS database connection properties
		appDataSource.setJdbcUrl("jdbc:mysql://aaoi40350a2bqx.cu5eqkm0hiav.eu-west-2.rds.amazonaws.com:3306/stock_tracker_api?useSSL=false");
		appDataSource.setUser("stockapi");
		appDataSource.setPassword("stockapiTest");
		
		// connection pool properties set
		appDataSource.setInitialPoolSize(10);
		appDataSource.setMinPoolSize(10);
		appDataSource.setMaxPoolSize(30);		
		appDataSource.setMaxIdleTime(2950);

		return appDataSource;
	}
	
	private Properties getHibernateProperties() {

		// sets the properties for Hibernate 
		Properties appProperties = new Properties();

		appProperties.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect");
		appProperties.setProperty("hibernate.show_sql", "true");
		
		return appProperties;				
	}
	
	
	@Bean
	public LocalSessionFactoryBean sessionFactory(){
		
		// creates an instance of session factory
		LocalSessionFactoryBean sF = new LocalSessionFactoryBean();
		
		// sets the properties and the packages to scan
		sF.setDataSource(appDataSource());
		sF.setPackagesToScan("com.mmu17097655.stocktracker.entity");
		sF.setHibernateProperties(getHibernateProperties());
		
		return sF;
	}
	
	@Bean
	@Autowired
	public HibernateTransactionManager transactionManager(SessionFactory sessionFactory) {
		
		// setup transaction manager based on session factory
		HibernateTransactionManager txManager = new HibernateTransactionManager();
		txManager.setSessionFactory(sessionFactory);

		return txManager;
	}	
	
	// sets up the configuration to allow access to the resources folder for assetts such as images.
	@Override
    public void addResourceHandlers(ResourceHandlerRegistry hsr) {
        hsr.addResourceHandler("/resources/**")
           .addResourceLocations("/resources/"); 
    }
	
}









