package com.mmu17097655.stocktracker.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * This class contains necessary variables, constructors, getters and setters relevant for a product in the database.
 * 
 * @author linton
 * @version 1.0
 * @since 2018-09-12
 *
 */
@Entity
@Table(name="product")
public class Product {
	
	// VARIABLES
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="id")
	private int id;
	
	@Column(name="item_name")
	private String itemName;
	
	@Column(name="item_type")
	private String itemType;
	
	@Column(name="item_location")
	private String itemLocation;
	
	@Column(name="item_colour")
	private String itemColour;
	
	@Column(name="item_size")
	private String itemSize;
	
	@Column(name="item_spec")
	private String itemSpec;
	
	@Column(name="beacon")
	private int beacon;
	
	@Column(name="customer")
	private String customer;
	
	public Product() {
		
	}
	
	// Constructor for the update Location method
	public Product(String itemLocation, int beacon) {
		super();
		this.itemLocation = itemLocation;
		this.beacon = beacon;
	}

	// Full argument constructor
	public Product(int id, String itemName, String itemType, String itemLocation, String itemColour, String itemSize,
			String itemSpec, int beacon, String customer) {
		super();
		this.id = id;
		this.itemName = itemName;
		this.itemType = itemType;
		this.itemLocation = itemLocation;
		this.itemColour = itemColour;
		this.itemSize = itemSize;
		this.itemSpec = itemSpec;
		this.beacon = beacon;
		this.customer = customer;
	}

	// GETTERS & SETTERS

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemLocation() {
		return itemLocation;
	}

	public void setItemLocation(String itemLocation) {
		this.itemLocation = itemLocation;
	}
	
	public String getItemColour() {
		return itemColour;
	}

	public void setItemColour(String itemColour) {
		this.itemColour = itemColour;
	}

	public String getItemSize() {
		return itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getItemSpec() {
		return itemSpec;
	}

	public void setItemSpec(String itemSpec) {
		this.itemSpec = itemSpec;
	}

	public int getBeacon() {
		return beacon;
	}

	public void setBeacon(int beacon) {
		this.beacon = beacon;
	}

	public String getCustomer() {
		return customer;
	}

	public void setCustomer(String customer) {
		this.customer = customer;
	}

	
	@Override
	public String toString() {
		return "Product [id=" + id + ", itemName=" + itemName + ", itemType=" + itemType + ", itemLocation="
				+ itemLocation + ", itemColour=" + itemColour + ", itemSize=" + itemSize + ", itemSpec=" + itemSpec
				+ ", beacon=" + beacon + ", customer=" + customer + "]";
	}

	
}
