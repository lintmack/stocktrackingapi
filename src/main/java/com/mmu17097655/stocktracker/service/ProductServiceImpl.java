package com.mmu17097655.stocktracker.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mmu17097655.stocktracker.dao.ProductDAO;
import com.mmu17097655.stocktracker.entity.Product;

/**
 * 
 * ProductServiceImpl is an extension of the ProductService interface to add a layer between the Service layer and the DAO.
 * 
 * @author linton
 * @version 1.0
 * @since 2018-09-18
 *
 */
@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductDAO productDAO; 
	
	@Override
	@Transactional
	public void saveProduct(Product theProduct) {
		productDAO.saveProduct(theProduct);

	}

	@Override
	@Transactional
	public List<Product> getProducts() {
		return productDAO.getProducts();
	}

	@Override
	@Transactional
	public Product getProduct(int productId) {
		return productDAO.getProduct(productId);
	}

	@Override
	@Transactional
	public void deleteProduct(int productId) {
		productDAO.deleteProduct(productId);
	}
	
	@Override
	@Transactional
	public List<Product> searchProducts(String searchId){
		return productDAO.searchProducts(searchId);
	}

	@Override
	@Transactional
	public void updateLocation(Product theProduct) {
		productDAO.updateLocation(theProduct);
		 
	}

}
