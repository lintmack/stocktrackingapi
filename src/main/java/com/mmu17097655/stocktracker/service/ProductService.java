package com.mmu17097655.stocktracker.service;

import java.util.List;
/**
 * 
 * ProductService is an interface, designed to add an extra layer to decouple the application further.
 * 
 * @author linton
 * @version 1.0
 * @since 2018-09-18
 *
 */

import com.mmu17097655.stocktracker.entity.Product;

public interface ProductService {
	
	public List<Product> getProducts();
	public void saveProduct(Product theProduct);
	public Product getProduct(int productId);
	public void deleteProduct(int productId);
	List<Product> searchProducts(String searchId);
	public void updateLocation(Product theProduct);


}
