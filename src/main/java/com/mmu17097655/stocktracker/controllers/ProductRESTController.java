package com.mmu17097655.stocktracker.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.mmu17097655.stocktracker.entity.Product;
import com.mmu17097655.stocktracker.service.ProductService;

@RestController
@RequestMapping("/api")

/**
 * 
 * This class acts as a Controller to perform REST methods.
 * 
 * @author linton
 * @version 1.0
 * @since 2018-09-12
 *
 */
public class ProductRESTController {

	@Autowired
	private ProductService productService;


	/**
	 * Method to save a new product to the database using http REST POST.
	 * @param product This is the only parameter for the method.
	 * @return Product returns the product in JSON format.
	 *
	 */
	@PostMapping("/put")
	@ResponseStatus(HttpStatus.ACCEPTED)
	public Product addProduct(@RequestBody Product theProduct) {
		theProduct.setId(0);
		productService.saveProduct(theProduct);
		return theProduct;
	}

	/**
	 * Method to return list of products from the database using http REST GET.
	 * @param No parameters for this method.
	 * @return List This returns a list of products in the database in JSON.
	 *
	 */
	@GetMapping("/products")
	public List<Product> getProducts() {
		return productService.getProducts();
	}

	/**
	 * Method to return a specific product from the database using http REST GET.
	 * @param productId requires a single parameter for the method.
	 * @return Product This returns a single product from the database in JSON.
	 */
	@GetMapping("/products/{productId}")
	public Product getProduct(@PathVariable int productId) {
		Product theProduct = productService.getProduct(productId);
		return theProduct;
	}
	
	/**
	 * Method to update an existing product to the database using http PUT.
	 * @param product This is the only parameter for the method.
	 * @return Product returns the product in JSON format
	 *
	 */
	@PutMapping("/products")
	public Product updateProduct(@RequestBody Product theProduct) {
		productService.saveProduct(theProduct);
		return theProduct;
	}
	
	/**
	 * Method to delete a specific product from the database using http REST DELETE.
	 * @param productId requires a single parameter for the method.
	 * @return String This method returns a string confirming the entry was deleted.
	 *
	 */
	@DeleteMapping("/products/{productId}")
	public String deleteProduct(@PathVariable int productId) {
		productService.deleteProduct(productId);
		return "Product with id - " + productId + " DELETED";
	}
	
	/**
	 * Method to update the location of a product in the database using http REST PATCH.
	 * @param Product requires a product object as a parameter.
	 * @return Product method returns Product in JSON format.
	 *
	 */
	@PatchMapping("/products")
	public Product updateLocation(@RequestBody Product theProduct) {
		System.out.println("XXXX-ID: "+theProduct.getBeacon());
		productService.updateLocation(theProduct);
		return theProduct;
	}

}
