package com.mmu17097655.stocktracker.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.mmu17097655.stocktracker.entity.Product;
import com.mmu17097655.stocktracker.service.ProductService;

@Controller
@RequestMapping("/")
/**
 * 
 * Class is used as a Test Controller for the application.
 * 
 * @author linton
 * @version 1.0
 * @since 2018-09-12
 *
 */
public class TestController {
	
	@Autowired
	private ProductService productService;

	
		@GetMapping("/products")
		public String getProducts(Model theModel) {
			List<Product> theProducts = productService.getProducts();
			theModel.addAttribute("products", theProducts);
			return "list";
		}
		
		@GetMapping("/addProductForm")
		public String showAddProductForm(Model model) {
		Product theProduct = new Product();
		model.addAttribute("products", theProduct);
			return "add-product-form";
		}
		
		@PostMapping("saveProduct")
		public String saveProduct(@ModelAttribute("products") Product theProduct) {
			productService.saveProduct(theProduct);
			return "redirect:/products";
		}
		
		@GetMapping("/showUpdateProductForm")
		public String showUpdateProductForm(@RequestParam("productId") int productId, Model model) {
			Product theProduct = productService.getProduct(productId);
			model.addAttribute("products", theProduct);
			return "add-product-form";
		}
		
		@GetMapping("/deleteProduct")
		public String deleteProduct(@RequestParam("productId") int productId) {
			productService.deleteProduct(productId);
			return "redirect:/products";
		}
		
		@PostMapping("/searchProducts")
		public String searchProducts(@RequestParam("searchId") String searchId, Model model) {
			List <Product> theProduct = productService.searchProducts(searchId);
			model.addAttribute("products", theProduct);
			return "list";
		}
}
