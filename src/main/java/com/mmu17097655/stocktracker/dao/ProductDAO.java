package com.mmu17097655.stocktracker.dao;

import java.util.List;

import com.mmu17097655.stocktracker.entity.Product;
/**
 * 
 * ProductDAO is an interface, which links to the productDAOImp class, which contains the 
 * CRUD methods.
 * 
 * @author linton
 * @version 1.0
 * @since 2018-09-18
 *
 */

public interface ProductDAO {
	
	public List<Product> getProducts();
	
	public void saveProduct(Product theProduct);

	Product getProduct(int productId);

	public void deleteProduct(int productId);

	List<Product> searchProducts(String searchId);

	public void updateLocation(Product theProduct);


}
