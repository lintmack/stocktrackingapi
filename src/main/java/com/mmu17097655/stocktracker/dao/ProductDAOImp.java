package com.mmu17097655.stocktracker.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mmu17097655.stocktracker.entity.Product;

/**
 * 
 * This class contains all the CRUD methods for the application.
 * 
 * @author linton
 * @version 1.0
 * @since 2018-09-12
 *
 */

@Repository
public class ProductDAOImp implements ProductDAO {

	@Autowired
	private SessionFactory sF;

	/**
	 * Method to save a new product or update an existing product to the database.
	 * @param product This is the only parameter for the method.
	 * @return void Nothing to return
	 *
	 */
	@Override
	public void saveProduct(Product theProduct) {
		Session activeSession = sF.getCurrentSession();
		activeSession.saveOrUpdate(theProduct);
	}

	/**
	 * Method to return list of products from the database.
	 * @param No parameters for this method.
	 * @return List This returns a list of products in the database.
	 *
	 */
	@Override
	public List<Product> getProducts() {
		Session currentSession = sF.getCurrentSession();
		Query<Product> query = currentSession.createQuery("from Product", Product.class);
		List<Product> products = query.getResultList();
		return products;
	}
	
	/**
	 * Method to return a specific product from the database.
	 * @param productId requires a single parameter for the method.
	 * @return Product This returns a single product from the database.
	 *
	 */
	@Override
	public Product getProduct(int productId) {
		Session currentSession = sF.getCurrentSession();
		Product theProduct = currentSession.get(Product.class, productId);
		
		return theProduct;
	}
	
	/**
	 * Method to delete a specific product from the database.
	 * @param productId requires a single parameter for the method.
	 * @return void This method is void so returns nothing.
	 *
	 */
	@Override
	public void deleteProduct(int productId) {
		Session currentSession = sF.getCurrentSession();
		Query query = currentSession.createQuery("delete from Product where id=:productsId");
		query.setParameter("productsId", productId);
		query.executeUpdate();		
	}
	
	/**
	 * Method to return a List of products based upon id from the database.
	 * @param productId requires a single parameter for the method.
	 * @return List This returns a list of products from the database.
	 *
	 */
	@Override
	public List <Product> searchProducts(String searchId){
		Session currentSession = sF.getCurrentSession();
		int productId = Integer.parseInt(searchId);
		Query query = null;
		if(productId > 0) {
			query = currentSession.createQuery("from Product p where p.id =:itemName");
			query.setParameter("itemName", productId);
		} else if (productId <= 0) {
			query = currentSession.createQuery("from Product");
		} 
		List<Product> products = query.getResultList();
		return products;
	}

	/**
	 * Method to update the location of a product in the database.
	 * @param Product requires a product object as a parameter.
	 * @return void method is void so returns nothing.
	 *
	 */
	@Override
	public void updateLocation(Product theProduct) {
			Session currentSession = sF.getCurrentSession();
			Query query=currentSession.createQuery("update Product set itemLocation=:itemLocation where beacon=:beacon");
			query.setParameter("itemLocation",theProduct.getItemLocation());
			query.setParameter("beacon",theProduct.getBeacon());
			query.executeUpdate();
	}

}
