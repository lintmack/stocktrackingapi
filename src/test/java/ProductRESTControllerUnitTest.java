import java.util.List;

import javax.net.ssl.SSLEngineResult.Status;

import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.http.*;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mmu17097655.stocktracker.controllers.ProductRESTController;
import com.mmu17097655.stocktracker.entity.Product;
import com.mmu17097655.stocktracker.service.ProductService;

public class ProductRESTControllerUnitTest {

	private MockMvc mockMvc;
	public List<Product> products = Arrays.asList(
			new Product(1, "9R", "Kayak", "WW1", "Blue", "Medium", "Stout", 1, "STOCK"),
			new Product(2, "9R", "Kayak", "WW5", "Orange", "Large", "Stout", 2, "PYR1"));

	@Mock
	private ProductService productService;

	@InjectMocks
	private ProductRESTController productRESTController;

	@Before
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(productRESTController).build();

	}

	@Test
	public void getAllProducts_Test_Successfull() throws Exception {

		when(productService.getProducts()).thenReturn(products);
		mockMvc.perform(get("/api/products")).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$", hasSize(2))).andExpect(jsonPath("$[0].id", is(1)))
				.andExpect(jsonPath("$[0].itemName", is("9R"))).andExpect(jsonPath("$[0].itemType", is("Kayak")))
				.andExpect(jsonPath("$[0].itemSize", is("Medium"))).andExpect(jsonPath("$[0].itemColour", is("Blue")))
				.andExpect(jsonPath("$[0].itemSpec", is("Stout"))).andExpect(jsonPath("$[0].itemLocation", is("WW1")))
				.andExpect(jsonPath("$[0].beacon", is(1))).andExpect(jsonPath("$[0].customer", is("STOCK")))
				.andExpect(jsonPath("$[1].id", is(2))).andExpect(jsonPath("$[1].itemName", is("9R")))
				.andExpect(jsonPath("$[1].itemType", is("Kayak"))).andExpect(jsonPath("$[1].itemSize", is("Large")))
				.andExpect(jsonPath("$[1].itemColour", is("Orange"))).andExpect(jsonPath("$[1].itemSpec", is("Stout")))
				.andExpect(jsonPath("$[1].itemLocation", is("WW5"))).andExpect(jsonPath("$[1].beacon", is(2)))
				.andExpect(jsonPath("$[1].customer", is("PYR1")));

		verify(productService, times(1)).getProducts();
		verifyNoMoreInteractions(productService);

	}

	@Test
	public void getAProduct_Test_Successfull() throws Exception {
		Product product = new Product(1, "Shiva", "Kayak", "WW3", "Green", "Medium", "Stout", 1, "STOCK");

		when(productService.getProduct(1)).thenReturn(product);

		mockMvc.perform(get("/api/products/{productId}", 1)).andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
				.andExpect(jsonPath("$.id", is(1))).andExpect(jsonPath("$.itemName", is("Shiva")))
				.andExpect(jsonPath("$.itemType", is("Kayak"))).andExpect(jsonPath("$.itemSize", is("Medium")))
				.andExpect(jsonPath("$.itemColour", is("Green"))).andExpect(jsonPath("$.itemSpec", is("Stout")))
				.andExpect(jsonPath("$.itemLocation", is("WW3"))).andExpect(jsonPath("$.beacon", is(1)))
				.andExpect(jsonPath("$.customer", is("STOCK")));

		verify(productService, times(1)).getProduct(1);
		verifyNoMoreInteractions(productService);
	}

	@Test
	public void createProduct_Test_Successful() throws Exception {
		Product product = new Product(1, "Burn", "Kayak", "Medium", "Orange", "CR", "W", 1, "STOCK");

		doNothing().when(productService).saveProduct(product);

		mockMvc.perform(
				post("/api/put").contentType(MediaType.APPLICATION_JSON).content(displayProductAsJsonString(product)))
				.andExpect(status().isAccepted());

		ArgumentCaptor<Product> capture = ArgumentCaptor.forClass(Product.class);

		verify(productService, times(1)).saveProduct(capture.capture());
		verifyNoMoreInteractions(productService);

		Product product2 = capture.getValue();

		assertThat(product2.getItemName(), is("Burn"));
	}
	
//	@Test 
//	public void UpdateProduct_Test_Successful() throws Exception{
//		Product product = new Product(1, "Burn", "Kayak", "Medium", "Orange", "CR", "W", 1, "STOCK");
//
//		doNothing().when(productService).saveProduct(product);
//
//		mockMvc.perform(
//				post("/api/products").contentType(MediaType.APPLICATION_JSON).content(displayProductAsJsonString(product)))
//				.andExpect(status().isAccepted());
//
//		ArgumentCaptor<Product> capture = ArgumentCaptor.forClass(Product.class);
//
//		verify(productService, times(1)).saveProduct(capture.capture());
//		verifyNoMoreInteractions(productService);
//
//		Product product2 = capture.getValue();
//
//		assertThat(product2.getItemName(), is("Burn"));
//	}
//	
	

	public static String displayProductAsJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	public Product findById(int id) {
		for (Product product : products) {
			if (product.getId() == id) {
				return product;
			}
		}
		return null;
	}

	public boolean productExists(Product product) {
		return findById(product.getId()) != null;
	}

}
